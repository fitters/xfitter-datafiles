# Process settings
sroot        = 38.8 # Center-of-mass energy
ih1          = 1    # Hadron 1: 1 for proton, -1 for antiproton
ih2          = 1    # Hadron 2: 1 for proton, -1 for antiproton
nproc        = 3    # Process: 1) W+; 2) W-; 3) Z/gamma*

# Settings for nuclear PDFs
nuclearpdf = true
Z1          = 1 #0.46031746 #29    # Beam 1 atomic number
Z2          = 0.46031746      # Beam 2 atomic number
A1          = 1 #63    # Beam 1 atomic mass number
A2          = 1     # Beam 2 atomic mass number

# Perturbative order
# fixedorder_only = true   # Evaluate predictions at fixed order
# fixedorder_only = false  # Evaluate predictions including qt-resummation
fixedorder_only = false
order           = 4        # QCD order: 0) LO(+LL), 1) NLO(+NLL), 2) NNLO(+NNLL), 3) N3LO(+N3LL), 4) N3LO(+N4LLa)
primed          = true     # Switch for primed or unprimed orders (NLL'/NLL NNLL'/NNLL N3LL'/N3LL N4LL'/N4LL)
qed             = true    # QED and QED-QCD mixed corrections ON/OFF
qedorder        = 1        # QCD order: 0) LO(+LL), 1) NLO(+NLL), 2) NNLO(+NNLL) Mixed QCD+QED

# Options for setting the order of different parts of the calculation
order_sudak     = -1
order_hcoef     = -1
order_evol      = -1
order_expc      = -1
order_ct        = 2
order_vjet      = 2

qedorder_sudak     = -1
qedorder_hcoef     = -1
qedorder_evol      = -1
qedorder_expc      = -1
qedorder_ct        = -1
qedorder_vjet      = -1

# QED and QED-QCD mixed settings
alpha0 = false # Use alpha(0) in the Sudakov and in the H coefficients

# N3LO settings
qbox = true     # Switch for quark box contribution at N3LO
qtriang = false # Switch for quark triangle contribution (implemented only in the analytic V+J at O(as^2))

# N4LL uncertainties (variations should be computed in the range [-1,1]
A5       = 0 # Set to a positive (negative) value <k> to vary the A5 coefficient by a factor <k> times its plus (minus) uncertainty
B4       = 0 # Set to a positive (negative) value <k> to vary the B4 coefficient by a factor <k> times its plus (minus) uncertainty
gam4NSp  = 0 # Set to a positive (negative) value <k> to vary the non-singlet plus 4-loop splitting functions by a factor <k> times its plus (minus) uncertainty
gam4NSm  = 0 # Set to a positive (negative) value <k> to vary the non-singlet minus 4-loop splitting functions by a factor <k> times its plus (minus) uncertainty
gam4NSv  = 0 # Set to a positive (negative) value <k> to vary the non-singlet valence 4-loop splitting functions by a factor <k> times its plus (minus) uncertainty
gam4SGqq = 0 # Set to a positive (negative) value <k> to vary the singlet qq 4-loop splitting functions by a factor <k> times its plus (minus) uncertainty
gam4SGqg = 0 # Set to a positive (negative) value <k> to vary the singlet qg 4-loop splitting functions by a factor <k> times its plus (minus) uncertainty
gam4SGgq = 0 # Set to a positive (negative) value <k> to vary the singlet gq 4-loop splitting functions by a factor <k> times its plus (minus) uncertainty
gam4SGgg = 0 # Set to a positive (negative) value <k> to vary the singlet gg 4-loop splitting functions by a factor <k> times its plus (minus) uncertainty
H4       = 0 # Set to a positive (negative) value <k> to vary the H4qqdelta coefficient by a factor <k>
C4qq     = 0 # Set to a positive (negative) value <k> to vary the C4qq coefficients by a factor <k>
C4qg     = 0 # Set to a positive (negative) value <k> to vary the C4qg coefficients by a factor <k>
C4qqb    = 0 # Set to a positive (negative) value <k> to vary the C4qqb coefficients by a factor <k>
C4qqp    = 0 # Set to a positive (negative) value <k> to vary the C4qqp coefficients by a factor <k>
C4qqbp   = 0 # Set to a positive (negative) value <k> to vary the C4qqbp coefficients by a factor <k>

# Strategy for the approximation of the singlet 4-loop splitting functions:
#-1 Singlet approximations from arxiv:2302.07593, arxiv:2307.04158, arxiv:2310.05744, arxiv:2404.09701
#0  MSHT20 approximation (uncertainty from variations of the overall coefficient functions)
#1  MSHT20 approximation (uncertainty from MSHT20 variations)
#2  Levin transform
#3  Weninger-delta transform
#4  Levin transform on gam/gamLO;
sgapprox = -1


# Non-perturbative form factor, S_NP = exp(-npff(b))
# 0: Gaussian (BLNY)                 npff(b) = (g1 + g1x*(x1^g1a*(1-x1)^g1b + x2^g1a*(1-x2)^g1b) + g2*log(m/Q0) + g3*log(100*m/sqrt(s)))*b^2
# 1: Exponential                     npff(b) = e*b
# 2: Collins-Rogers                  npff(b) = g1/sqrt(1+lambda*b^2)*b^2 + sign(q)*(1-exp(-abs(q)*b^4)) + sx*b^6 + g0*(1-exp(-(Cf*alphas(b0/bstar))*b^2)/(PI*g0*blim^2))*ln((m/Q0)^2)
# 3: Dokshitzer-Marchesini-Webber    npff(b) = exp(-(a2 * (log(m)+1.) - a2p) * b^2/2);
# 4: Pavia19                         
# 5: SV19                         
npff = 2

# Dokshitzer-Marchesini-Webber options (npff = 3)
a2 = 0.2
a2p = 0.0


# Collins-Rogers options (npff = 2)
g1     = 0.6 # Gaussian term
lambda = 1.0   # Transition to exponential
q      = -0.2   # Quartic term
sx     = 0   # Sextic term
g0     = 0.72 # g0 at blim = bref
bref   = 1.5 # Reference value of blim for the running of g0(blim)
Q0     = 1   # Starting scale of the TMD evolution

# Pavia19 (npff = 4)
lambda = 0.580
g2A = 0.036
g2B = 0.012
NA = 0.625
sigmaA = 0.370
alphaA = 0.205
NB = 0.044
sigmaB = 0.356
alphaB = 0.069
Q0 = 1

x
flavour_kt   = false       # Flavour-dependent g1 parameters
g1_uv = 0.5  # u-valence
g1_us = 0.5  # u-sea
g1_dv = 0.5  # d-valence
g1_ds = 0.5  # d-sea
g1_ss = 0.5  # strange
g1_ch = 0.5  # charm
g1_bo = 0.5  # bottom
g1_gl = 0.5  # gluon

gk = false

# PDF settings
LHAPDFset    = MSHT20nnlo_as118  # PDF set from LHAPDF
LHAPDFmember = 0                 # PDF member

# Running of alphas
alphaslha = false  # If true, take alphas(mur) from LHAPDF, otherwise alphas is evolved from alphas(mZ) to alphas(mur) at (order+1)-loops
orderlha  = true   # If true, set the order of the PDF evolution in the Sudakov equal to the order of the PDF evolution in LHAPDF
blimlha   = false  # If true, set blim_pdf not larger than b0/Qmin
murpdf    = true   # If true, include renormalisation scale variations in the PDF evolution

# Functional form of QCD scales (mV: wmass or zmass, pT: boson transverse momentum, mjj: dijet invariant mass)
#0: mu^2 = mV^2
#1: mu^2 = mll^2
#2: mu^2 = mll^2+pT^2
#3: mu^2 = mll^2+pT^2+mjj^2
#4: mu^2 = (pT + sqrt(mll^2+pT^2))^2
#5: mu^2 = mll^2+2*pT^2
fmuren = 1     # Functional form of the renormalisation scale
fmufac = 1     # Functional form of the factorisation scale
fmures = 1     # Functional form of the resummation scale (forms >= 2 are equivalent to 1, because mures is evaluated at pT = 0)

# QCD scale settings
kmuren = 1.0 #0.5         # Scale factor for the renormalisation scale
kmufac = 1.0 #0.5         # Scale factor for the factorisation scale
kmures = 1.0 #0.5         # Scale factor for the resummation scale

#PDF matching scales
kmuc          = 1.        # Scale factor for the charm matching scale
kmub          = 1.        # Scale factor for the bottom matching scale
kmut          = 1.        # Scale factor for the top matching scale

# EW scheme
#0: Input: alpha(mZ), zmass, xw;     Derived: wmass, Gf
#1: Input: Gf, wmass, zmass;         Derived: xw, alpha(mZ)    [Gmu scheme]
#2: Input: Gf, alpha(mZ), xw;        Derived: wmass, zmass
#3: All masses and couplings determined by inputs
#4: All masses and couplings determined by inputs, couplings evaluated assuming Gauge invariance (as in MCFM)
ewscheme = 1

# EW parameters
Gf    = 1.1663787e-5         # G-Fermi
zmass = 91.1876              # Mass of the Z boson
wmass = 80.385               # Mass of the W boson
xw    = 0.23153              # Weak-mixing angle    (not used in the Gmu scheme)
aemmz = 7.7585538055706e-03  # alpha_EM(MZ)         (not used in the Gmu scheme)

# W and Z total widths used in the propagator are determined by the following inputs
zwidth = 2.4950          # Width of the Z boson
wwidth = 2.091           # Width of the W boson

# Fixed width / Running width options
runningwidth = false     # Use Z and W propagators including energy-dependent width effects
conv2fixw = false        # Convert Z and W masses and widths from running width to fixed width values

# CKM matrix
Vud = 0.97427
Vus = 0.2253
Vub = 0.00351
Vcd = 0.2252
Vcs = 0.97344
Vcb = 0.0412

# Z/gamma* coupling to quarks
Zuu = 1.0
Zdd = 1.0
Zss = 1.0
Zcc = 1.0
Zbb = 1.0

# Include virtual photon and interference in Z/gamma* production
useGamma = true

# Resummation parameters
qtcutoff = 0.02   # Resummation cutoff in GeV

# Resum canonical logarithms L = log(Q^2/mub^2) with mub = b0/b, or modified logarithms L~ = log(Q^2/mubtl^2)
#0: Canonical logarithms L = log( (Q*b/b0)^2 )
#1: Modified logarithms L~ = log( (Q*b/b0)^2 + 1)
#2: Modified logarithms L~ = 1/p*log( (Q*b/b0)^2*p + 1) with p = 4
#3: Modified logarithms L~ = log(Q^2/mubtl^2) with mubtl = mub * pow(1.-exp(-pow(Q/mub,p)),1./p)
#4: Modified logarithms with profile scales:  mubtl = Q*(1+g(x)*(y-1)), x = qt/Q, y = mub/Q
#5: Modified logarithms L~ = log(Q^2/mubtl^2) with mubtl = Q*exp(-int_0^Q {dx/x * (1-J0(b*x))})
modlog = 1

# Switches for exponentiation of C coefficients
#0: Switch off all C coefficients in the exponentiation
#1: Exponentiate only delta terms of Cnqq coefficients in the exponentiation
#2: Exponentiate only flavour diagonal part
#3: Full exponentiation with regularisation term (option expcreg)
#4: Do not expand the denominator in the exponentiation of off-diagonal terms, keep only terms up to C<order-1>
#5: Do not expand the denominator in the exponentiation of off-diagonal terms
#6: Evaluate C(as) at as(mub)
#7: Taylor expansion of the N-dependent coefficients up to order ntaylor

# Main expc switch
expc = 3

# Options for expc
ntaylor = 5
expcreg = 2

# Settings for the Sudakov form factor
# Running coupling solution in the Sudakov form factor
# 0: Approximate analytic expanded solution for the running of alphas, expansion at fixed order in as(muR) 
# 1: Approximate analytic expanded solution for the running of alphas, expansion at fixed order in as(q)
# 2: Approximate analytic iterative solution for the running of alphas, expansion of 1/as(q)
# 3: Exact numerical solution for the running of alphas with LQR variations
# 4: Exact numerical solution for the running of alphas with RGE-invariant Csi variations
# 5: VFN running coupling with CRunDec3
asrun = 0

# Technical settings for the Sudakov form factor
sumlogs = false  # Sum all logs before exponentiation (helpful with bprescription = 0, but create issues with bprescription = 2)
numsud  = false  # Integrate the Sudakov form factor numerically (otherwise use the analytical solution)
numexpc = false  # Integrate the C exponentiation numerically (otherwise use the analytical solution)

# Heavy flavour massive corrections
deltaismc = false
deltaismb = false
deltafsmc = false
deltafsmb = false
vfncqg    = false

# Use the CMW scheme
cmw  = false   # CMW scheme in the resummed part
cmwfo = false  # CMW scheme in the finite order part

# Prescription to avoid the Landau pole in the Bessel inverse transform
# 0: bstar prescription, which freezes b at bmax: b -> bstar(b) = b/sqrt(1+b^2/bmax^2)
# 1: Integrate all the way up to the Landau singularity b_L = b0/Q * exp(1/(2*beta0*alphas))
# 2: Minimal prescription (complex plane)
# 3: Minimal prescription (real axis)
# 4: Local bstar prescription (can be used only with numsud = true)
bprescription = 0

# Functional form of the bstar prescription
# 0: bstar = b/sqrt(1+b^2/bmax^2)
# 1: bstar = bmax*(1-exp(b^6/bmax^6))^1/6
fbstar = 0

#Value of blim for the bstar prescription. Positive values set a fixed bmax=blim, a negative values sets bmax=b_L/(-blim), where b_L is the Landau singularity.
blim         = 1.5  # Main blim setting
blim_pdf     = 0  # If not zero, set a different blim for the PDF evolution
blim_sudakov = 0  # If not zero, set a different blim for the Sudakov form factor
blim_expc    = 0  # If not zero, set a different blim for the exponentiation of the C coefficients

bstar_pdf     = false  # Force bstar prescription for the PDF evolution
bstar_sudakov = false  # Force bstar prescription for the Sudakov form factor
bstar_expc    = false  # Force bstar prescription for the exponentiation of the C coefficients

phibr = 4.        #set arg(z) as phib = pi/phibr  for the integration contour in the complex plane for bprescription = 2 (should be set phibr > 4. )
bcf = 0.5         #select the point bc = bcf * b_L, where the integration contour is bended in the complex plane, as a fraction of the Landau singularity b_L. Applies to bprescription = 2 or 3

# Settings for the inverse Hankel transform
bintaccuracy = 1.0e-4  # Accuracy of the integration

# Strategy for the direct Mellin transform of PDFs at the factorization scale
#0: Numerical integration with Gauss-Legendre quadrature
#1: Approximation in x-space with Laguerre polynomials
#2: Approximation with google ceres
mellintr = 0

# Set to 1 to use the dyres approximation of PDFs and integration contour in the complex plane for the Mellin inversion
# Set to 0 to use exact PDFs and straight line contour in the complex plane for the Mellin inversion
opts_approxpdf = 0

# x-to-N direct Mellin transform of PDFs
opts_pdfintervals = 100   # Number of intervals for integration of PDF moments
pdfrule = 200             # Gaussian rule for the x-to-N Mellin transform

# Type of PDF evolution
#0: FFN five-flavour backward evolution (implemented up to NLO)
#1: FFN five-flavour backward evolution with Pegasus-QCD (implemented up to NNLO)
#2: VFN direct Mellin transform from LHAPDF
#3: VFN with Pegasus-QCD
#4: FFN five-flavour backward evolution numerical (allows inclusion of small-x with HELL)
#5: FFN five-flavour backward evolution truncated or iterative (implemented up to N3LO)
evolmode = 2

# For evolmode 1 or 5 use the iterative or the truncated solution
iterative = false

# For evolmode 5 switch off bottom and charm PDFs below the corresponding thresholds
hfmode = 2

# Small-x resummation in the PDF evolution (with HELL)
smallx = false
nsub = 100

# Comute PDF evolution U(mub,Q) = U(muF,Q)*U(mub,muF)
commute = true

# Settings for the inverse Mellin integrations
mellininv = 0       # Strategy for the Mellin inversion (0 Gauss-Legendre, 1 Talbot)
mellinintervals = 1 # Number of intervals
mellinrule = 12     # Number of nodes (should be increased, when increasing ncycle below)

# Options for the Mellin inversion with Gauss-Legendre integration
zmax = 2   	    # Upper limit of the contour in the imaginary axis
ncycle = 10         # Number of pi-cycles in the contour (use 40 for mellin1d = false at high rapidity)
cpoint = 3   	    # Point of intersection of the contour with the real axis (scaled by 1/log(z))
cshift = 0.5	    # Shift to the right of the point of intersection of the contour with the real axis
phi = 0.5           # Angle between the real axis and the linear contour in units of pi

mellincores = 1     # Number of parallel threads for the Mellin integration (not yet supported)
mellin1d = true     # Use 1d (y-integrated) or 2d (y-dependent) Mellin inversion
melup = 2           # Strategy for the update of the contour for the Mellin inversion (0; do not update; 1: update in each bin; 2: update at each phase-space point)
xspace = false      # Access PDFs in Bjorken-x space, without Mellin inversion (option available only in the LL case where the convolution is trivial)

# Resummation damping
damp = false

# Resummation damping function
# 1: Gaussian:    exp(-(k*mll-qt)^2)/(delta*mll)^2
# 2: Exponential: exp((k*mll)^-qt^2)/(delta*mll)^2
# 3: Cosine:      cos(PI/(delta*mll)*(qt-k*mll))+1)/2
dampmode = 1
dampk = 0.75
dampdelta = 0.25

# Resummation scheme
# 0: DY scheme               (Hq = 0)
# 1: Hard scheme             (H1q = 2.*C1qq_delta)
# 2: Semi Hard scheme        (H1q = C1qq_delta)
# 3: CSS scheme              (H1q = CF*(pi2/2.-4.))
# 4: NS (non-singlet) scheme (H1q = CF*(pi2/2.-7./2.))
resscheme = 0

# qt-subtraction cut-off. Both conditions are applied, at least one between qtcut and xqtcut must be > 0
xqtcut = 0.008  # cutoff on qt/m
qtcut = 0.      # cutoff on qt

# qt cut-off for the fiducial power corrections
qtfpc = 1e-4 # FPC cutoff on qt/m

# Integration settings
rseed        = 123456         # Random seed for MC integration

#cut off on invariant mass between emitted and radiator in V+jet
mcutoff = 1e-3

#Nagy parameters for V+jet
aii = 1
aif = 1
afi = 1
aff = 1

# Term switches
doBORN = true
doCT   = true
doVJ   = true
doFPC  = true

doVJREAL = true
doVJVIRT = true

# Integration type: true -> quadrature, false -> vegas
BORNquad = true
CTquad   = true
VJquad   = true
FPCquad  = true

# Integration type (advanced settings, override BORNquad, CTquad, VJquad, FPCquad options if set > -1)
intDimRes = 0  # Resummed term (1, 2 or 3 for quadrature, or 4 for vegas)
intDimBorn = -1 # Born term (2 for quadrature, 4 or 6 for vegas)
intDimCT = 0   # Counter term (1, 2 or 3 for quadrature, or 6, or 8 for vegas)
intDimVJ = 3   # V+jet term (3 for quadrature, 5 for quadrature with cuts, or 7 for vegas)
intDimFPC = -1  # FPC term (2 for quadrature, 8 for vegas)

# Multithreading model
threading     = 0   # Multithreading model (0 for openmp, 1 for fork/wait)

# Multithreading parallelisation
cores         = 22   # Number of parallel threads (0 for turning off parallelisation)

# Cuba settings
cubaverbosity     = 0     # Cuba info messsages, from 0 to 3
cubanbatch        = 1000  # The batch size for sampling in Cuba vegas integration
niterBORN         = 5     # Only for 2d and 3d cuhre integration of resummed part
niterCT           = 5     # Only for 2d and 3d cuhre integration of counter term
niterVJ           = 10    # Only for 3d cuhre integration of V+J

#Vegas settings
vegasncallsBORN   = 1000     # only for res 4d vegas integration
#vegasncallsRES    = 1000     # only for res 4d vegas integration
vegasncallsCT     = 100000   # only for 6d and 8d vegas integration of the counter term
vegasncallsVJLO   = 10000000 # only for lo 7d vegas integration
vegasncallsVJREAL = 10000000 # only for real 10d vegas integration
vegasncallsVJVIRT = 1000000  # only for virt 8d vegas integration
vegascollect      = true     # collect points from all the vegas iterations (true) or only from the last iteration (false)
vegascorr         = false    # drop vegas importance sampling and perform only one vegas sweep (allows having fully correlated vegas phase space points)

# cubature settings
pcubature = true   # Use Cuhre (false ) or pcubature (true) integration in quadrature mode
relaccuracy = 1e-3 # target relative uncertainty of each term
absaccuracy = 0    # target absolute uncertainty of each term in fb

# Smolyak sparse grids level
level = 2

# Advanced integration settings

# Gaussian rule for the invariant mass integration in the 0dim resummed piece
mrule = 3

# Number of intervals and gaussian rule for the rapidity integrations in the 2dim resummed piece
yintervals = 1
yrule = 64

# Number of intervals and gaussian rule for the qt integration in the 2dim counter term
qtintervals = 1
qtrule = 64

# Number of intervals and gaussian rule for the alfa beta scaled-PDF integration in the counter term and born fixed order term
abintervals = 1
abrule = 64

# Gaussian rule for the phi integration in the V+J 5d LO term when makecuts is false
vjphirule = 20

# Settings for the z1, z2 integration in the V+J 3d NLO term
zrule = 64

# Settings for the x integration in the V+J 3d delta term
xrule = 200

# xF cuts
xfmin = -0.1
xfmax = +0.2

# costh CS boundaries
costhmin = -1
costhmax = +1

# Lepton cuts
# Total cross section or with lepton cuts
makecuts = false

# charged leptons cuts
lptcut = 20
lycut = 2.5 # absolute rapidity cut

# leptons and antileptons cuts
lepptcut = 0
lepycut = 1000
alpptcut = 0
alpycut = 1000

#absolute-rapidity-ordered leptons (central and forward)
lcptcut = 0
lcymin = 0
lcymax = 1000
lfptcut = 0
lfymin = 0
lfymax = 1000

# cuts on neutrino and transverse mass (these cuts are applied only in W processes)
etmisscut = 0
mtcut = 0

#costh CS
cthCSmin = -1
cthCSmax = +1

# integration types and settings for costh phi_lep phase space
cubaint   = false   # integration with Cuba Suave
trapezint = false  # trapezoidal rule for the phi_lep integration and semi-analytical for costh
quadint   = true  # quadrature rule for the phi_lep integration and semi-analytical for costh

suavepoints  = 1000000 # number of points for suave integration, newpoints is set to suavepoints/10;
nphitrape    = 1000    # number of steps for trapezoidal rule of phi_lep integration
phirule      = 4       # quadrature rule of phi_lep integration
phiintervals = 20      # number of segments for quadrature rule of phi_lep integration
ncstart      = 100     # starting sampling for the costh semi-analytical integration (common settings for the trapezoidal and quadrature rules)

# qt-recoil prescriptions
qtrec_naive = false
qtrec_cs    = true
qtrec_kt0   = false

# Debug settings
timeprofile = false  # debug and time profile resummation integration
verbose     = false  # debug and time profile costh phi_lep integration

# Output settings
output_filename = results  # output filename
texttable   = true         # dump result table to text file
redirect    = false        # redirect stdout and stderr to log file (except for gridverbose output)
unicode     = false        # use unicode characters for the table formatting
silent      = false        # no output on screen (except for gridverbose output)
makehistos  = true         # fill histograms
gridverbose = false        # printout number of events to keep job alive when running on grid

# Compute total (-1) or helicity cross sections (0-7)
helicity = -1

# binning

# normalise cross sections by bin width
ptbinwidth = true
ybinwidth = false
mbinwidth = false

dsdqt2 = true
dsdxf  = true
edsdp3 = true

# Force to loop over all bins even you have all Vegas integrands
force_binsampling = false

# boson qt, y, m bins
qt_bins = [ 0.0 0.2 0.4 0.6 0.8 1.0 1.2 1.4 ]
y_bins = [ -10 10 ]
m_bins = [ 7 8 ]

# lepton pt, eta bins (experimental, do not use)
ptl_bins  = [ 0 13000 ]
etal_bins = [ -10 10 ]

# default histograms (need to be run in vegas mode)
hetal = false
hptl  = false
hptnu = false
hmt   = false

# Angular coefficients histograms (need to be run in vegas mode)
haiqt = false
haiy  = false
haim  = false

etalep_bins = [ 0.0 0.2 0.4 0.6 0.8 1.0 1.2 1.4 1.6 1.8 2.0 2.2 2.4 ]
ptlep_bins  = [ 30 35 40 45 50 55 60 ]
ptnu_bins   = [ 30 35 40 45 50 55 60 ]
mt_bins     = [ 60 65 70 75 80 85 90 95 100 ]

# binning for user testing histogram
biganswer_bins = [ 41 42 43 ]
