LHeC pseudodata used in xFitter paper 1907.01014:

 1) LHeC_charmCC_dummy_em.dat: dummy pseudodata for e-p reaction with many Q2, x point used to produce theory predictions in Figs. 1-10 (note that accessing particular partonic channels (Figs. 8-10) is not available in xFitter outof-the-box but require some simple manipulations with PDFs from LHAPDF)
 
 2) LHeC_charmCC_FFABM_em.dat, LHeC_charmCC_FONLLB_em.dat: LHeC pseudodata originally from 1206.2913 with central values set to predictions from FFABM (referred to as FFNS A in the paper) or FONLL-B schemes; stat. errors were adjusted accordingly
 
The difference between 1) and 2) is that 1) provides only bin information needed to get theoretical predictions (but dat and uncertainties are dummy), while 2) has meaningful uncertainties (as expected in LHeC measurements), and data values were set to certain theoretical predictions, thus 2) can be used for PDF profiling
