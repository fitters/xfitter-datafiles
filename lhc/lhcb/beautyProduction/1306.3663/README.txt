*
* The data are taken from LHCB-PAPER-2013-004
* "Measurement of B meson production cross-sections in proton-proton collisions at sqrt(s) = 7 TeV"
* JHEP 1308 (2013) 117
* arXiv:1306.3663
*
* These data were originally added in 2015 for PROSA analysis arXiv:1503.04581 [Eur.Phys.J. C75 (2015) 396]
* Files with -cbdiff- are added in 2019 for PROSA analysis arXiv:1911.13164 and use 'cbdiff' reaction
*
