*
* The data are taken from LHCb-PAPER-2015-041
* "Measurements of prompt charm production cross-sections in pp collisions at sqrt(s) = 13 TeV"
* JHEP 1603 (2016) 159, Erratum: JHEP 1609 (2016) 013, Erratum: JHEP 1705 (2017) 074
* arXiv:1510.01707
*
* These data were added for PROSA analysis arXiv:1911.13164 and use 'cbdiff' reaction
*
