*
* The data are taken from LHCb-PAPER-2012-041
* "Prompt charm production in pp collisions at sqrt(s)=7 TeV"
* Nucl. Phys. B 871 (2013), 1
* arXiv:1302.2864
*
* These data were originally added in 2015 for PROSA analysis arXiv:1503.04581 [Eur.Phys.J. C75 (2015) 396]
* Files with -cbdiff- are added in 2019 for PROSA analysis arXiv:1911.13164 and use 'cbdiff' reaction
*
