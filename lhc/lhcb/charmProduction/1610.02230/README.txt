*
* The data are taken from LHCB-PAPER-2016-042 
* "Measurements of prompt charm production cross-sections in pp collisions at sqrt(s) = 5 TeV"
* JHEP 1706 (2017) 147
* arXiv:1610.02230
*
* These data were added for PROSA analysis arXiv:1911.13164 and use 'cbdiff' reaction
*
