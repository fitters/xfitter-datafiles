*
* The data are taken from CERN-EP-2017-020
* "Measurement of D-meson production at mid-rapidity in pp collisions at sqrt(s) = 7 TeV"
* Eur.Phys.J. C77 (2017) no.8, 550
* arXiv:1702.00766
* These data are complementary to LHCb measurement arXiv:1306.3663
* Normalised cross sections (files with _Normy) are calculated by normalising to the corresponding LHCb measurement (bin 3 < y < 3.5)
*
* These data were added for PROSA analysis arXiv:1911.13164 and use 'cbdiff' reaction
*
