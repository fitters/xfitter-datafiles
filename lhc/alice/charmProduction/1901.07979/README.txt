*
* The data are taken from CERN-EP-2019-004
* "Measurement of D0, D+, D*+ and D+s production in pp collisions at sqrt(s) = 5.02 TeV with ALICE"
* Eur.Phys.J. C79 (2019) no.5, 388
* arXiv:1901.07979
* These data are complementary to LHCb measurement arXiv:1610.02230
* Normalised cross sections (files with _Normy) are calculated by normalising to the corresponding LHCb measurement (bin 3 < y < 3.5)
*
* These data were added for PROSA analysis arXiv:1911.13164 and use 'cbdiff' reaction
*
